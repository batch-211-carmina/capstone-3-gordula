import React, { useContext, useEffect, useState } from 'react';
import { Button, Form } from 'react-bootstrap';
import { Helmet } from 'react-helmet-async';
import { useNavigate } from 'react-router-dom';
import CheckoutSteps from '../components/CheckoutSteps';
import { Store } from '../Store';

export default function Shipping() {
  const navigate = useNavigate();

  const { state, dispatch: ctxDispatch } = useContext(Store);

  const {
    userInfo,
    cart: { shippingAddress },
  } = state;

  const [fullName, setFullName] = useState(shippingAddress.fullName || '');
  const [address, setAddress] = useState(shippingAddress.address || '');
  const [deliveryTime, setDeliveryTime] = useState(
    shippingAddress.deliveryTime || ''
  );

  useEffect(() => {
    if (!userInfo) {
      navigate('/login?redirect=/shipping');
    }
  }, [userInfo, navigate]);

  const submitHandler = e => {
    e.preventDefault();

    ctxDispatch({
      type: 'SAVE_SHIPPING_ADDRESS',
      payload: {
        fullName,
        address,
        deliveryTime,
      },
    });
    localStorage.setItem(
      'shippingAddress',
      JSON.stringify({
        fullName,
        address,
        deliveryTime,
      })
    );
    navigate('/payment');
  };

  return (
    <div className="vh-100 div-container">
      <Helmet>
        <title>Carmaj Swittut | Shipping Address</title>
      </Helmet>
      <CheckoutSteps step1 step2></CheckoutSteps>
      <div className="container small-container">
        <h2 className="mt-5 mb-3 text-center">Shipping Address</h2>
        <Form onSubmit={submitHandler}>
          <Form.Group className="mb-3" controlId="fullName">
            <Form.Label>Full Name</Form.Label>
            <Form.Control
              value={fullName}
              onChange={e => setFullName(e.target.value)}
              required
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="address">
            <Form.Label>Complete Address</Form.Label>
            <Form.Control
              value={address}
              onChange={e => setAddress(e.target.value)}
              required
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="deliveryTime">
            <Form.Label className="mb-3">Delivery Time</Form.Label>
            <Form.Select
              value={deliveryTime}
              onChange={e => setDeliveryTime(e.target.value)}
              required
            >
              <option>Select Available Delivery Time</option>
              <option value="12:00:00">12:00 PM</option>
              <option value="13:00:00">01:00 PM</option>
              <option value="14:00:00">02:00 PM</option>
              <option value="15:00:00">03:00 PM</option>
              <option value="16:00:00">04:00 PM</option>
              <option value="17:00:00">05:00 PM</option>
              <option value="18:00:00">06:00 PM</option>
            </Form.Select>
          </Form.Group>
          <div className="mb3">
            <Button type="submit" className="shipping-btn">
              Continue
            </Button>
          </div>
        </Form>
      </div>
    </div>
  );
}
