import { Row, Col, Button } from 'react-bootstrap';
import { Helmet } from 'react-helmet-async';
import { Link } from 'react-router-dom';

export default function NotFound() {
  return (
    <div className="div-container">
      <Helmet>
        <title>Carmaj Swittut | Not Found Page</title>
      </Helmet>
      <Row className="mt-3 mb-3">
        <Col className="text-center">
          <h1>404 - Page Not Found</h1>
          <p>The requested URL was not found on this server.</p>
          <Button className="not-found-btn" as={Link} to="/">
            Back to Home
          </Button>
        </Col>
      </Row>
    </div>
  );
}
