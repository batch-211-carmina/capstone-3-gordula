import React from 'react';
import { Col, Row } from 'react-bootstrap';

export default function Footer() {
  return (
    <div className="container-fluid footer-container mt-5 ">
      <Row>
        <Col md={4}>
          <h6 className="mt-5 mb-3">BRANCH INFORMATION</h6>
          <div>
            <p>
              <i className="fa fa-clock"></i>
              <span className="ms-3">OPEN HOUR: 9:00 - 18:00</span>
            </p>
            <p>
              <i className="fa fa-location-dot"></i>
              <span className="ms-3"> Bangkok, Thailand</span>
            </p>
            <p>
              <i className="fa fa-envelope"></i>
              <span className="ms-3">carmajswittut@gmail.com</span>
            </p>
            <p>
              <i className="fa fa-phone"></i>
              <span className="ms-3">0837063201</span>
            </p>
          </div>
        </Col>
        <Col md={4} className="mt-5">
          <div className="footer-icons d-flex justify-content-center mt-5">
            <p className="me-3">
              <i className="fa-brands fa-square-facebook "></i>
            </p>
            <p>
              <i className="fa-brands fa-square-instagram"></i>
            </p>
          </div>
          <div className="text-center follow-us">Follow Us!</div>
        </Col>
        <div className="text-center my-3">
          &#169; 2022 Carmaj Swittut. All rights reserved
        </div>
      </Row>
    </div>
  );
}
